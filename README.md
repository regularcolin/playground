## Instructions - Running from built source

```
docker build -t somewebapp .
docker run -p 8080:80 --name somewebapp somewebapp
```
browse to: http://localhost:8080/weatherforecast or just http://localhost:8080/

## Instructions - Running from docker hub

```
docker run -p 8080:80 --name somewebapp regularcolin/playground
```
browse to: http://localhost:8080/weatherforecast or just http://localhost:8080/

## Instructions - Running from kubernetes

```
kubectl apply -f deployment.yml --record
kubectl apply -f service.yml
```

browse to: http://<nodeip>:30001/weatherforecast or just http://<nodeip>:30001



## What is this?

This is a very simple ASP.NET Core web application. It's essentially a pre-made sample
generated by Visual Studio, modified to insert some very simple xunit unit tests into it.

This repo was built to experiment with CI/CD and serves as an example of how to do that.
This repo has the following features:

* Any commits to any branch will result in BitBucket Pipelines automatically compiling the code, running unit tests and building the docker container
* Any commits to master will additinally be pulled by docker hub:  https://hub.docker.com/repository/docker/regularcolin/playground/

These principals show how it's possible to build an application in a way that minimises the time from code commit to deployment,
while also allowing for quality gates to minmise risk.