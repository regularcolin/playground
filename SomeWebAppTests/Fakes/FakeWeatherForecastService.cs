﻿using SomeWebApp;
using System;
using System.Collections.Generic;
using System.Text;

namespace SomeWebAppTests.Fakes
{
    class FakeWeatherForecastService : IWeatherForecastService
    {
        private readonly List<WeatherForecast> _forecasts;

        public FakeWeatherForecastService()
        {
            _forecasts = new List<WeatherForecast>
            {
                new WeatherForecast{ Date = new DateTime(2019,1,1), TemperatureC = 5, Summary = "it's gonna snow"},
                new WeatherForecast{ Date = new DateTime(2019,2,1), TemperatureC = 7, Summary = "it's gonna hail"},
                new WeatherForecast{ Date = new DateTime(2019,3,1), TemperatureC = 12, Summary = "it's gonna rain"},
                new WeatherForecast{ Date = new DateTime(2019,4,1), TemperatureC = 18, Summary = "it's a fine day"},
                new WeatherForecast{ Date = new DateTime(2019,5,1), TemperatureC = 31, Summary = "it's crazy hot"},
            };
        }

        public IEnumerable<WeatherForecast> GetSummaries()
        {
            return _forecasts;
        }
    }
}
