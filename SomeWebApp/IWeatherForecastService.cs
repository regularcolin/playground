﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeWebApp
{
    public interface IWeatherForecastService
    {
        public IEnumerable<WeatherForecast> GetSummaries();
    }
}
