﻿using SomeWebApp;
using System;
using System.Collections.Generic;
using System.Text;

namespace SomeWebAppTests.Comparers
{
    public class ForecastComparer : IEqualityComparer<WeatherForecast>
    {
        public bool Equals(WeatherForecast a, WeatherForecast b)
        {
            return (a.Date == b.Date) &&
                   (a.Summary == b.Summary) &&
                   (a.TemperatureC == b.TemperatureC);
        }

        public int GetHashCode(WeatherForecast wf)
        {
            int hash = Convert.ToInt32(wf.Date.Ticks) + wf.Summary.GetHashCode() + wf.TemperatureC;
            return hash.GetHashCode();
        }
    }
}
