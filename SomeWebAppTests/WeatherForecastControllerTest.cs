using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using SomeWebApp;
using SomeWebApp.Controllers;
using SomeWebAppTests.Comparers;
using SomeWebAppTests.Fakes;
using System;
using System.Collections.Generic;
using Xunit;

namespace SomeWebAppTests
{

    public class WeatherForecastControllerTest
    {
        WeatherForecastController _controller;
        IWeatherForecastService _service;
        ILogger<WeatherForecastController> _logger;

        public WeatherForecastControllerTest()
        {
            
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("LoggingConsoleApp.Program", LogLevel.Debug)
                    .AddConsole();
            });
            
            _logger = loggerFactory.CreateLogger<WeatherForecastController>();

            _service = new FakeWeatherForecastService();
            _controller = new WeatherForecastController(_logger, _service);
        }

        [Fact]
        public void Does_Not_Crash()
        {
            // Act
            var result = _controller.Get();
        }

        [Fact]
        public void Returns_Value_From_FakeWeatherForecastService()
        {
            // Arrange
            List<WeatherForecast> expected = new List<WeatherForecast>
            {
                new WeatherForecast{ Date = new DateTime(2019,1,1), TemperatureC = 5, Summary = "it's gonna snow"},
                new WeatherForecast{ Date = new DateTime(2019,2,1), TemperatureC = 7, Summary = "it's gonna hail"},
                new WeatherForecast{ Date = new DateTime(2019,3,1), TemperatureC = 12, Summary = "it's gonna rain"},
                new WeatherForecast{ Date = new DateTime(2019,4,1), TemperatureC = 18, Summary = "it's a fine day"},
                new WeatherForecast{ Date = new DateTime(2019,5,1), TemperatureC = 31, Summary = "it's crazy hot"},
            };

            // Act
            var result = _controller.Get();

            //Assert
            Assert.Equal(expected, result, new ForecastComparer());
        }
    }
}
